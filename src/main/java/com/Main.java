package com;

import com.shapes.Circle;
import com.shapes.Rectangle;
import com.shapes.Triangle;


public class Main {
    public static void main(String[] args) {
        Circle Blue_Circle = new Circle();
        Blue_Circle.setColor("Blue");
        Circle Red_Circle = new Circle();
        Red_Circle.setColor("Red");
        Circle Yellow_Circle = new Circle();
        Yellow_Circle.setColor("Yellow");
        Rectangle Blue_Rectangle = new Rectangle();
        Blue_Rectangle.setColor("Blue");
        Rectangle Red_Rectangle = new Rectangle();
        Red_Rectangle.setColor("Red");
        Rectangle Yellow_Rectangle = new Rectangle();
        Yellow_Rectangle.setColor("Yellow");
        Triangle Grey_Triangle = new Triangle();
        Grey_Triangle.setColor("Grey");
        Triangle Purple_Triangle = new Triangle();
        Purple_Triangle.setColor("Purple");
        Triangle Orange_Triangle = new Triangle();
        Orange_Triangle.setColor("Orange");

    }
}
