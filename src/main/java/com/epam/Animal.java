package com.epam;

abstract public class Animal {
    void sleep() {
        System.out.println("I am sleeping");
    }

    void eat() {
        System.out.println("I eat anything");
    }
}
